package org.beifengtz.jvmm.web.mvc.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.beifengtz.jvmm.web.entity.po.LogGcPO;

/**
 * Description: TODO
 *
 * Created in 10:32 2022/2/28
 *
 * @author beifengtz
 */
@Mapper
public interface LogGcMapper extends BaseMapper<LogGcPO> {
}
